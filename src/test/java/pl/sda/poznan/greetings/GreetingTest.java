package pl.sda.poznan.greetings;

import org.junit.Test;
import pl.sda.poznan.Greeting;

import static junit.framework.TestCase.assertEquals;

public class GreetingTest {
//
//    @Test
//    public void is_greeting_by_name_works(){
//        String name = "Jan";
//        String helper = String.valueOf(greet(name));
//        assertEquals("Hello, Jan", helper);
//    }
//
//    @Test
//    public void is_empty_name_supported(){
//        String name = null;
//        String helper = String.valueOf(greet(name));
//        assertEquals("Hello, my friend", helper);
//    }
    @Test
    public void should_greet_for_single_name(){
        String greet = Greeting.greet("Jan");
        assertEquals("Hello, Jan", greet);
    }

    @Test
    public void should_greet_as_a_friend_when_null_is_passed(){
        String greet = Greeting.greet(null);
        assertEquals("Hello, my friend", greet);
    }

    @Test
    public void should_return_greet_in_capital_letters(){
        String greet = Greeting.greet("JAN");
        assertEquals("HELLO, JAN", greet);
    }

    @Test
    public void should_greet_two_people(){
        String greet = Greeting.greet("Jan", "Ala");
        assertEquals("Hello, Jan and Ala", greet);
    }

    @Test
    public void should_greet_many_people(){
        String greet = Greeting.greet("Piotr", "Anna", "Paulina");
        assertEquals("Hello, Piotr, Anna and Paulina", greet);
    }
}