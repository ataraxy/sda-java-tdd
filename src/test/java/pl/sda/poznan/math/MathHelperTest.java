package pl.sda.poznan.math;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;


public class MathHelperTest {

//    int number;
//    int factorial;
//
//    public MathHelperTest(int number, int factorial) {
//        this.number = number;
//        this.factorial = factorial;
//    }
//
//    @Parameterized.Parameters
//    public static Collection<Object[]> data() {
//        return Arrays.asList(new Object[][]{
//                {1, 1},
//                {2, 2},
//                {3, 6},
//                {4, 24},
//                {5, 120}
//        });
//    }
//
//    @Test
//    public void is_result_of_factorial_correct() {
//        assertEquals
//    }

    @Test
    public void should_return_one() {
        /// AAA
        /// Arrange
        int n = 0;

        //Act
        int factorial = MathHelper.factorial(n);

        //Assert
        assertEquals(1, factorial);
    }

    @Test
    public void should_return_result_when_one_is_passed() {
        int n = 1;
        int result = MathHelper.factorial(n);
        assertEquals(1, result);
    }

    @Test
    public void should_calculate_factorial() {
        int n = 5;
        int factorial = MathHelper.factorial(n);
        assertEquals(120, factorial);
    }

    @Test(expected = IllegalArgumentException.class)
    public void should_throw_exception_on_negative_value() {
        int n = -5;
        int result = MathHelper.factorial(n);
    }
}