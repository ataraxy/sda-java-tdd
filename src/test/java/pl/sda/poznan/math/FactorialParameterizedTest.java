package pl.sda.poznan.math;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;


@RunWith(Parameterized.class)
public class FactorialParameterizedTest {

    private int n;
    private int expectedValue;

    public FactorialParameterizedTest(int n, int expectedValue) {
        this.n = n;
        this.expectedValue = expectedValue;
    }

    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {2, 2},
                {3, 6},
                {4, 24},
                {5, 120},
        });
    }

    @Test
    public void calculate_factorial_test(){
        // AAA
        // Act
        int result = MathHelper.factorial(this.n);

        // Assert
        assertEquals(this.expectedValue, result);
    }
}
