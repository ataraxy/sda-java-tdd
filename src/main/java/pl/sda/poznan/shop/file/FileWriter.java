package pl.sda.poznan.shop.file;

import pl.sda.poznan.shop.model.Product;

import java.io.IOException;
import java.util.List;

public interface FileWriter<E> {
    void saveToFile(List<E> elements) throws IOException;
}
